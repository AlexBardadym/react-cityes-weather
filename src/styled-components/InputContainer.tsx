import styled from "styled-components";

export const InputContainer = styled.div`
  margin: 50px 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;
