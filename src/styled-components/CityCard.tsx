import styled from "styled-components";

export const CityCardBox = styled.div`
  position: relative;
  width: 300px;
  height: 450px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  &:hover {
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
    width: 305px;
    height: 455px;
  }
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 30px;
  .close-btn {
    position: absolute;
    right: -15px;
    top: -15px;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: rgb(220, 0, 78);
    cursor: pointer;
    color: white;
  }
`;

export const EmptyCard = styled.div`
  width: 300px;
`;
