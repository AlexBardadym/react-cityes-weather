import React, { Component } from "react";
import axios from "axios";

import CityCard from "./components/city-card/CityCard";
import { CityesContainer } from "./styled-components/CitiesContainer";
import CityInput from "./components/input/Input";
import { EmptyCard } from "./styled-components/CityCard";

interface AppState {
  cityesList: Array<string>;
  cityesWeather: Array<object>;
}

class App extends Component<{}, AppState> {
  state = {
    cityesList: [],
    cityesWeather: []
  };

  getAllCityesWaether = async () => {
    const API_KEY: string = "886f4b9dd54f43ef1f0ae2586db5104b";
    const promises = this.state.cityesList.map(city => {
      return axios(
        `https://cors-anywhere.herokuapp.com/https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`
      );
    });
    await Promise.all(promises)
      .then(results =>
        results.map(item => {
          return item.data;
        })
      )
      .then(data =>
        this.setState({ cityesWeather: data }, () => console.log(this.state))
      );
  };

  componentDidMount() {
    if (this.state.cityesWeather.length !== 0) {
      this.getAllCityesWaether();
    }
  }

  addCityToList = (name: string) => {
    const newCityesList = this.state.cityesList;
    newCityesList.push(name);
    this.setState(
      {
        cityesList: newCityesList
      },
      () => this.getAllCityesWaether()
    );
  };

  removeCityFromList = (name: string) => {
    const newCityesList = this.state.cityesList;
    const index: number = newCityesList.indexOf(name);
    newCityesList.splice(index, 1);
    this.setState({
      cityesList: newCityesList
    });
  };

  render() {
    return (
      <>
        <CityInput addCity={this.addCityToList} />
        <CityesContainer>
          {this.state.cityesList.map((name, i) => {
            return (
              <CityCard
                weatherInfo={{}}
                removeCity={this.removeCityFromList}
                key={`${name}-city-num-${i}`}
                name={name}
              />
            );
          })}
          <EmptyCard />
        </CityesContainer>
      </>
    );
  }
}

export default App;
