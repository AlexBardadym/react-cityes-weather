import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";

import { InputContainer } from "../../styled-components/InputContainer";

interface State {
  name: string;
}

interface Props {
  addCity: (data: string) => void;
}

class CityInput extends Component<Props, State> {
  state = {
    name: ""
  };

  handleChange = () => (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      name: event.target.value
    });
  };

  makeSubmit = () => {
    this.props.addCity(this.state.name);
    this.setState({
      name: ""
    });
  };

  render() {
    return (
      <InputContainer>
        <TextField
          id="city-input"
          label="Enter city"
          value={this.state.name}
          onChange={this.handleChange()}
          variant="outlined"
        />
        <Fab
          color="primary"
          aria-label="Add"
          size="small"
          style={{ marginLeft: 15 }}
          onClick={this.makeSubmit}
        >
          <AddIcon />
        </Fab>
      </InputContainer>
    );
  }
}

export default CityInput;
