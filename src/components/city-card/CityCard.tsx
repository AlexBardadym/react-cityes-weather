import React, { Component } from "react";
import DeleteIcon from "@material-ui/icons/Delete";

import { CityCardBox } from "../../styled-components/CityCard";

interface CytyProps {
  name: string;
  removeCity: (data: string) => void;
  weatherInfo: object;
}

class CytyCard extends Component<CytyProps> {
  deleteCity = () => this.props.removeCity(this.props.name);

  render() {
    return (
      <CityCardBox>
        <div onClick={this.deleteCity} className="close-btn">
          <DeleteIcon />
        </div>
        {this.props.name}
        <div />
      </CityCardBox>
    );
  }
}

export default CytyCard;
